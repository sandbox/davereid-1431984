(function ($) {

/**
 * Loads media browsers and callbacks, specifically for media as a field.
 */
Drupal.behaviors.dateField = {
  attach: function (context, settings) {
    // Support using Modernizr if installed.
    if (typeof Modernizr === 'undefined' || !Modernizr.inputtypes.date) {
      $('input[type="date"]').once('datefield-datepicker', function() {
        var options = {
          dateFormat: 'yy-mm-dd',
          showButtonPanel: true,
          changeYear: true
        };
        if ($(this).attr('min')) {
          options.minDate = $(this).attr('min');
        }
        if ($(this).attr('max')) {
          options.maxDate = $(this).attr('max');
        }
        $(this).datepicker(options);
      });
    }
  }
};

})(jQuery);
